package main;

import org.apache.velocity.app.VelocityEngine;
import spark.ModelAndView;
import spark.template.velocity.VelocityTemplateEngine;
import spark.utils.IOUtils;

import javax.servlet.MultipartConfigElement;
import javax.servlet.http.Part;
import java.io.InputStream;
import java.util.HashMap;
import java.util.zip.CRC32;

import static spark.Spark.get;
import static spark.Spark.post;

public class App {
    public static void main(String[] args) {
        VelocityEngine configuredEngine = new VelocityEngine();
        configuredEngine.setProperty("runtime.references.strict", true);
        configuredEngine.setProperty("resource.loader", "class");
        configuredEngine.setProperty("class.resource.loader.class",
                "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        VelocityTemplateEngine velocityTemplateEngine = new
                VelocityTemplateEngine(configuredEngine);
        get("/crc", (req, res) -> {
            HashMap<String, Object> model = new HashMap<>();
            String plain = req.queryParamOrDefault("plain", "");
            CRC32 crc = new CRC32();
            crc.update(plain.getBytes());
            model.put("crc", crc.getValue());
            String templatePath = "/views/crc.vm";
            return velocityTemplateEngine.render(new ModelAndView(model,
                    templatePath));
        });
        get("/crcFile", (req, res) -> {
            HashMap<String, Object> model = new HashMap<>();
            String templatePath = "/views/crc_file.vm";
            String crc = req.queryParamOrDefault("crc", "");
            model.put("crc", crc);
            return velocityTemplateEngine.render(new ModelAndView(model,
                    templatePath));
        });

        post("/crcFile", (req, res) -> {
            long crcResult = 0;
            try {
                req.attribute("org.eclipse.jetty.multipartConfig", new MultipartConfigElement(System.getProperty("user.dir")+"//tmp"));
                Part filePart = req.raw().getPart("myfile");
                if (filePart != null) {
                    try (InputStream inputStream =
                                 filePart.getInputStream()) {
                        byte[] bytes = IOUtils.toByteArray(inputStream);
                        CRC32 crc = new CRC32();
                        crc.update(bytes);
                        crcResult = crc.getValue();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            res.redirect("/crcFile?crc="+crcResult);
            return res;
        });
    }
}
